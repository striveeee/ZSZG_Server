package com.begamer.card.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Encoder;

/**
 * 加密解密
 * @author LiTao
 * 2013-8-16 11:12:43
 */
public class EncryptAndDecipher
{

	/**
	 * encrypt
	 * lt@2013-8-16 11:36:32
	 * @param userId
	 * @param msg 明文
	 * @return
	 */
	public static String encrypt(String key,String msg)
	{
		if(key==null || "".equals(key) || msg==null)
		{
			return msg;
		}
		if(key==null)
		{
			key="";
		}
		byte[] keyBytes=key.getBytes();
		byte[] msgBytes=null;
		try
		{
			msgBytes=msg.getBytes("UTF-8");
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		byte[] ciperBytes=new byte[msgBytes.length];
		for(int i=0;i<msgBytes.length;i++)
		{
			byte temp=keyBytes[i%keyBytes.length];
			if(i%2==0)
			{
				ciperBytes[i]=(byte)(msgBytes[i]+temp);
			}
			else
			{
				ciperBytes[i]=(byte)(msgBytes[i]-temp);
			}
		}
		return byte2hex(ciperBytes);
	}
	
	/**
	 * 解密
	 * lt@2013-8-15 下午08:38:07
	 * @param userId
	 * @param decipherMsg 密文
	 * @return
	 */
	public static String decipher(String key,String decipherMsg)
	{
		if(key==null || "".equals(key) || decipherMsg==null)
		{
			return decipherMsg;
		}
		byte[] keyBytes=key.getBytes();
		byte[] decipherBytes=hex2byte(decipherMsg);
		byte[] msgBytes=new byte[decipherBytes.length];
		for(int i=0;i<decipherBytes.length;i++)
		{
			byte temp=keyBytes[i%keyBytes.length];
			if(i%2==0)
			{
				msgBytes[i]=(byte)(decipherBytes[i]-temp);
			}
			else
			{
				msgBytes[i]=(byte)(decipherBytes[i]+temp);
			}
		}
		String result="";
		try
		{
			result=new String(msgBytes,"UTF-8");
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * generateKey
	 * lt@2013-8-16 11:39:35
	 * @param userId
	 * @return
	 */
	public static String generateKey()
	{
		String key="";
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] temp = md.digest(("chuangqian"+System.currentTimeMillis()+"meiyueguang").getBytes());
			BASE64Encoder encoder = new BASE64Encoder();
			key=encoder.encode(temp);
		} 
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return key;
	}
	
	/**
	 * 将二进制字节数组转化为十六进制字符串
	 * @param b 二进制字节数组
	 * @return String
	 */
	private static String byte2hex(byte[] b) 
	{
		String result="";
		for (int i=0;i<b.length;i++) 
		{
			String temp=Integer.toHexString(b[i]&0xff);
			if(temp.length()==1) 
			{
				temp="0"+temp;
			} 
			result=result+temp;
		}
		return result.toUpperCase();
	}
	
	/**
	 * 十六进制字符串转化为二进制字节数组
	 * @param hex
	 * @return
	 */
	private static byte[] hex2byte(String hex)
	{
		byte[] result=new byte[hex.length()/2];
		for (int i=0;i<result.length;i++) 
		{
			String temp="0x"+hex.charAt(i*2)+""+hex.charAt(i*2+1);
			result[i]=(byte)(int)Integer.decode(temp);
		}
		return result;
	}
	
	public static void main(String[] args)
	{
		String key=generateKey();
		try
		{
			String a="asfgsgdrhrtfjt我勒个去gdhtrjytkuil,./~!@#$%^&*()_+-=[]{};':|io;liuolio;5687569853435345678>?)(%#$)；了佛菩萨【卡高朋网入库高品位0-==-23";
			System.out.println("加密前明文:"+a);
			String b=encrypt(key, a);
			System.out.println("加密后密文:"+b);
			String c=decipher(key, b);
			System.out.println("解密后明文:"+c);
			System.out.println(a.equals(c));
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
