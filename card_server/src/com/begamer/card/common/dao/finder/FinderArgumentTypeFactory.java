package com.begamer.card.common.dao.finder;

import org.hibernate.type.Type;

public interface FinderArgumentTypeFactory {
	Type getArgumentType(Object arg);
}
