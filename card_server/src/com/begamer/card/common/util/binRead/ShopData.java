package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShopData implements PropertyReader
{
	public int id;
	public int goodstype;
	public int itemId;
	public String name;
	public int viplevel;
	public int dailynumber;
	public int totalnumber;
	public int costtype1;
	public int cost;
	
	private static HashMap<Integer, ShopData> data =new HashMap<Integer, ShopData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据id获取一个data**/
	public static ShopData getShopData(int index)
	{
		return data.get(index);
	}
	
	public static List<ShopData> getShopDatas1()
	{
		List<ShopData> list =new ArrayList<ShopData>();
		for(ShopData sd:data.values())
		{
			if(sd.dailynumber>0)
			{
				list.add(sd);
			}
		}
		return list;
	}
	
	public static List<ShopData> getShopDatas2()
	{
		List<ShopData> list =new ArrayList<ShopData>();
		for(ShopData sd:data.values())
		{
			if(sd.totalnumber>0)
			{
				list.add(sd);
			}
		}
		return list;
	}
}
