package com.begamer.card.common.util.binRead;

import java.util.HashMap;


public class ImaginationcomposeData implements PropertyReader 
{
	/**编号**/
	public int id;
	/**兑换类别:1,card 2,equipment 3,material 4,被动技能**/
	public int type;
	/**兑换物品**/
	public int composite;
	
	/**材料**/
	public int material1;
	/**数量**/
	public int number1;
	
	private static HashMap<Integer, ImaginationcomposeData> data =new HashMap<Integer, ImaginationcomposeData>();
	@Override
	public void addData() {
		data.put(id, this);
	}
	@Override
	public void parse(String[] ss) {
		
	}
	@Override
	public void resetData() {
		data.clear();
	}
	/**根据id获取一个ImaginationcomposeData**/
	public static ImaginationcomposeData getImaginationcomposeData(int index)
	{
		return data.get(index);
	}
	/**根据兑换物品id获取对应的data**/
	public static ImaginationcomposeData getDataByComposeId(int compose)
	{
		for(ImaginationcomposeData iData : data.values())
		{
			if(iData.composite ==compose)
			{
				return iData;
			}
		}
		return null;
	}
}
