package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RuneData implements PropertyReader
{
	/**编号**/
	public int id;
	/**属性**/
	public int proprety;
	/**数值**/
	public int value;
	/**消耗符文**/
	public int cost;
	/**成功率**/
	public int successrate;
	/**增长成功率**/
	public int upgrade;

	private static HashMap<Integer, RuneData> data=new HashMap<Integer, RuneData>();
	//==分组存放,key为id高3位,value为List<RuneData>==//
	private static HashMap<Integer, List<RuneData>> groupData=new HashMap<Integer, List<RuneData>>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
		
		int key=id/100;
		List<RuneData> list=groupData.get(key);
		if(list==null)
		{
			list=new ArrayList<RuneData>();
			groupData.put(key,list);
		}
		list.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
		groupData.clear();
	}

	public static RuneData getData(int id)
	{
		return data.get(id);
	}
	
	public static RuneData getNextData(int times,int page,int num)
	{
		List<RuneData> list=groupData.get(times*100+page);
		if(list.size()>num)
		{
			return list.get(num);
		}
		return null;
	}
	
	public static int getValues(int key,int num)
	{
		int result=0;
		List<RuneData> list=groupData.get(key);
		for(int k=0;k<num;k++)
		{
			result+=list.get(k).value;
		}
		return result;
	}
	
	public static int getValues(int key)
	{
		int result=0;
		List<RuneData> list=groupData.get(key);
		for (int i = 0; i < list.size(); i++)
		{
			result+=list.get(i).value;
		}
		return result;
	}
	
	public static int getGroupDataNum(int key)
	{
		return groupData.get(key).size();
	}
	
}
