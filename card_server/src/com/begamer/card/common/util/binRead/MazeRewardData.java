package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class MazeRewardData implements PropertyReader
{
	public int id;
	public List<String> reward;
	
	private static HashMap<Integer, MazeRewardData> data =new HashMap<Integer, MazeRewardData>();
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		
		reward =new ArrayList<String>();
		for(int i=0;i<7;i++)
		{
			location =1+3*i;
			int type=StringUtil.getInt(ss[location]);
			String itemID =StringUtil.getString(ss[location+1]);
			int pro =StringUtil.getInt(ss[location+2]);
			if(type !=0)
			{
				String mazeReward =type+"-"+itemID+"-"+pro;
				reward.add(mazeReward);
			}
			else
			{
				continue;
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static MazeRewardData getMazeRewardData(int index)
	{
		return data.get(index);
	}
	

}
