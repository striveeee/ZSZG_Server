package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RunData implements PropertyReader
{
	public int id;
	/**转动类型**/
	public int runtype;
	/**转动档位**/
	public int run;
	/**转动次数**/
	public int truntime;
	/**奖励物品类型**/
	public int rewardtype;
	/**奖励物品id,num***/
	public String timereward;
	
	private static HashMap<Integer, RunData> data =new HashMap<Integer, RunData>();
	private static List<RunData> dataList =new ArrayList<RunData>();
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static RunData getRunData(int index)
	{
		return data.get(index);
	}

	public static List<RunData> getDatasByType(int t)
	{
		List<RunData> rDatas =new ArrayList<RunData>();
		for(RunData rData:data.values())
		{
			if(rData.runtype==t)
			{
				rDatas.add(rData);
			}
		}
		return rDatas;
	}
	
	public static HashMap<Integer, RunData> getDataMap(int t)
	{
		HashMap<Integer, RunData> rMap =new HashMap<Integer, RunData>();
		for(RunData rData:data.values())
		{
			if(rData.runtype==t)
			{
				if(!rMap.containsKey(rData.id))
				{
					rMap.put(rData.id, rData);
				}
			}
		}
		return rMap;
	}
	public static List<RunData> getDatasByRun(int r)
	{
		List<RunData> rDatas =new ArrayList<RunData>();
		for(RunData rData:data.values())
		{
			if(rData.run==r)
			{
				rDatas.add(rData);
			}
		}
		return rDatas;
	}
}
