package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BloodValueData implements PropertyReader {
	// 编号id
	public int id;
	// 伤害区间
	public int damage;
	// 所获得金罡心
	public int get;
	
	private static HashMap<Integer, BloodValueData> data = new HashMap<Integer, BloodValueData>();
	private static List<BloodValueData> dataList = new ArrayList<BloodValueData>();
	
	@Override
	public void addData()
	{
		data.put(damage, this);
		dataList.add(this);
	}
	
	@Override
	public void parse(String[] ss)
	{
		
	}
	
	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}
	
	public static BloodValueData getData(int id)
	{
		return data.get(id);
	}
	
	public static List<BloodValueData> getDatas()
	{
		return dataList;
	}
	
	public static int getGet(int damage)
	{
		int n = 0;
		if (damage > 0)
		{
			for (int i = dataList.size() - 1; i >= 0; i--)
			{
				BloodValueData bvd = dataList.get(i);
				if (damage > bvd.damage)
				{
					n = bvd.get;
					break;
				}
			}
		}
		return n;
	}
}
