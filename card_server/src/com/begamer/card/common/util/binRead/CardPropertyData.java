package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class CardPropertyData implements PropertyReader
{

	public int level;
	public int atk;
	public int def;
	public int hp;

	private static HashMap<Integer, CardPropertyData> data=new HashMap<Integer, CardPropertyData>();
	
	@Override
	public void addData()
	{
		data.put(level, this);
	}

	public static CardPropertyData getData(int level)
	{
		return data.get(level);
	}
	
	@Override
	public void resetData()
	{
		data.clear();
	}

	@Override
	public void parse(String[] ss)
	{
	}

}
