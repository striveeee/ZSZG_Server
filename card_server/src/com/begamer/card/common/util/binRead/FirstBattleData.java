package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.CardJson;

public class FirstBattleData implements PropertyReader
{
	public int id;
	public int team;
	public String [] monsters;
	public int uniteskill1;
	public int energy1;
	public int uniteskill2;
	public int energy2;
	public List<String> uniteskill;
	
	private static HashMap<Integer, FirstBattleData> data =new HashMap<Integer, FirstBattleData>();
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location =0;
		id =StringUtil.getInt(ss[location]);
		team =StringUtil.getInt(ss[location+1]);
		monsters =new String[6];
		for(int k=0;k<6;k++)
		{
			location =2+k*7;
			int monster =StringUtil.getInt(ss[location]);
			int level =StringUtil.getInt(ss[location+1]);
			int atk =StringUtil.getInt(ss[location+2]);
			int def =StringUtil.getInt(ss[location+3]);
			int hp =StringUtil.getInt(ss[location+4]);
			int skill =StringUtil.getInt(ss[location+5]);
			int boss =StringUtil.getInt(ss[location+6]);
			String monsterInfo =monster+"-"+level+"-"+atk+"-"+def+"-"+hp+"-"+skill+"-"+boss;
			monsters[k] =monsterInfo;
		}
		uniteskill =new ArrayList<String>();
		for(int k=0;k<2;k++)
		{
			location=2+6*7+k*2;
			uniteskill1 =StringUtil.getInt(ss[location]);
			energy1 =StringUtil.getInt(ss[location+1]);
			String unite =uniteskill1+"-"+energy1;
			uniteskill.add(unite);
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据id获取data**/
	public static FirstBattleData getFirstBattleData(int index)
	{
		return data.get(index);
	}
	
	/**根据阵营team获取data**/
	public static FirstBattleData getFirstBattleDataByTeam(int t)
	{
		for(FirstBattleData fbData :data.values())
		{
			if(fbData.team == t)
			{
				return fbData;
			}
		}
		return null;
	}
	
	
	/**根据阵营获取cs**/
	public static CardJson[] getCs(int t)
	{
		FirstBattleData fbData =getFirstBattleDataByTeam(t);
		if(fbData==null)
		{
			return null;
		}
		CardJson[] cjs=new CardJson[6];
		for(int i=0;i<6;i++)
		{
			String[] ss=fbData.monsters[i].split("-");
			int cardId=StringUtil.getInt(ss[0]);
			int level=StringUtil.getInt(ss[1]);
			int atk=StringUtil.getInt(ss[2]);
			int def=StringUtil.getInt(ss[3]);
			int maxHp=StringUtil.getInt(ss[4]);
			int skillId=StringUtil.getInt(ss[5]);
			int bossMark=StringUtil.getInt(ss[6]);
			
			int criRate=0;
			int aviRate=0;
			int talent1=0;
			int talent2=0;
			int talent3=0;
			
			if(CardData.getData(cardId)==null)
			{
				continue;
			}
			cjs[i]=new CardJson(i, cardId, level, skillId, atk, def, maxHp, bossMark, criRate, aviRate, talent1, talent2, talent3);
		}
		return cjs;
	}
}
