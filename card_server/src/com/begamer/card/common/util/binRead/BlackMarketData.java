package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BlackMarketData implements PropertyReader
{
	public int id;
	public int goodstype;
	public int itemId;
	public String name;
	public int costtype;
	public int cost;
	public int number;
	public int level;
	public int showup;
	public int probability1;
	public int probability2;
	public int probability3;
	
	private static HashMap<Integer, BlackMarketData> data =new HashMap<Integer, BlackMarketData>();
	private static List<BlackMarketData> dataList =new ArrayList<BlackMarketData>();
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
		
	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	/**根据id获取一个data**/
	public static BlackMarketData getBlackMarketData(int index)
	{
		return data.get(index);
	}
	
	public static List<BlackMarketData> getDataList()
	{
		return dataList;
	}
}
