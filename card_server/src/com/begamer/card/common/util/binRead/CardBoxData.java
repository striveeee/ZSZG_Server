package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.Random;

/**
 * 卡库
 * @author LiTao
 * 2014-1-20 下午05:00:35
 */
public class CardBoxData implements PropertyReader
{
	/**编号**/
	public int id;
	/**抽卡类型ID**/
	public int typeID;
	/**卡牌ID**/
	public int cardID;
	/**几率**/
	public int probability;

	private static HashMap<Integer, List<CardBoxData>> data=new HashMap<Integer, List<CardBoxData>>();
	
	private static final int RandomSeed=10000;
	
	@Override
	public void addData()
	{
		//卡库类型集合
		List<CardBoxData> typeList=data.get(typeID);
		if(typeList==null)
		{
			typeList=new ArrayList<CardBoxData>();
			data.put(typeID, typeList);
		}
		typeList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	/**
	 * 根据卡库类型获取cardId
	 * lt@2014-1-20 下午05:14:45
	 * @param typeID
	 * @return
	 */
	public static int getCardIdByHouseType(int typeID)
	{
		List<CardBoxData> list=data.get(typeID);
		if(list!=null)
		{
			int roll=Random.getNumber(RandomSeed);
			for(CardBoxData cbd:list)
			{
				if(roll<cbd.probability)
				{
					return cbd.cardID;
				}
				roll-=cbd.probability;
			}
		}
		return 0;
	}
	
}