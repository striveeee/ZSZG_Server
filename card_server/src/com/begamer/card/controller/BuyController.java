package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.MailThread;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.Random;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.BlackMarketData;
import com.begamer.card.common.util.binRead.BlackShopBoxData;
import com.begamer.card.common.util.binRead.BloodCostData;
import com.begamer.card.common.util.binRead.EventData;
import com.begamer.card.common.util.binRead.GoldCostData;
import com.begamer.card.common.util.binRead.MissionCostData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.ShopData;
import com.begamer.card.common.util.binRead.ShoppvpData;
import com.begamer.card.common.util.binRead.SpiritShopData;
import com.begamer.card.common.util.binRead.VipData;
import com.begamer.card.common.util.binRead.VipGiftData;
import com.begamer.card.json.command2.BuyPowerOrGoldJson;
import com.begamer.card.json.command2.BuyPowerOrGoldResultJson;
import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.PkRank;

public class BuyController extends AbstractMultiActionController
{
	private static Logger errorlogger =ErrorLogger.logger;
	private static Logger playerlogger =PlayerLogger.logger;
	private static Logger logger=Logger.getLogger(BuyController.class);
	
	/**请求购买金币或者体力信息**/
	public ModelAndView buyPowerOrGoldInfo(HttpServletRequest request,HttpServletResponse response)
	{
		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			BuyPowerOrGoldJson bj =JSONObject.toJavaObject(jsonObject, BuyPowerOrGoldJson.class);
			BuyPowerOrGoldResultJson brj =new BuyPowerOrGoldResultJson();
			if(bj !=null)
			{
				PlayerInfo pi =Cache.getInstance().getPlayerInfo(bj.playerId, request.getSession());
				if(pi !=null)
				{
					playerlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|请求购买信息");
					VipData vData =VipData.getVipData(pi.player.getVipLevel());
					//校验阶段
					switch (bj.type)
					{
					case 1://金币
						//校验阶段
						int errorcode1 =0;
						if(pi.player.getBuyGoldTimes()>=vData.gold)//校验购买次数
						{
							errorcode1 =79;
						}
						if(errorcode1==0)
						{
							if(bj.jsonType==2)
							{
								if(pi.player.getBuyGoldTimes()>=vData.gold)//校验购买次数
								{
									errorcode1 =79;
								}
								//校验水晶或者金币是否足够
								GoldCostData gcd =GoldCostData.getGoldCostData(pi.player.getBuyGoldTimes()+1);
								if(gcd !=null)
								{
									if(bj.costType ==1)//花费水晶
									{
										if(pi.player.getTotalCrystal()<gcd.cost)
										{
											errorcode1 =19;
										}
									}
									else if(bj.costType==2)//花费金币
									{
										if(pi.player.getGold()<gcd.cost)
										{
											errorcode1 =19;
										}
									}
									else
									{
										errorcode1 =80;
										errorlogger.info("花费类型错误");
									}
								}
								else
								{
									errorlogger.info("花费金币data为null");
								}
							}
						}
						//功能阶段
						if(errorcode1 ==0)
						{
							if(bj.jsonType==1)//购买请求
							{
								GoldCostData gcd =GoldCostData.getGoldCostData(pi.player.getBuyGoldTimes()+1);
								brj.crystal =gcd.cost;
								brj.times =vData.gold-pi.player.getBuyGoldTimes();
								brj.num =gcd.number;
							}
							else if(bj.jsonType==2)//购买
							{
								GoldCostData gcd =GoldCostData.getGoldCostData(pi.player.getBuyGoldTimes()+1);
								pi.player.addGold(gcd.number);
								pi.player.setBuyGoldTimes(pi.player.getBuyGoldTimes()+1);
								if(bj.costType ==1)//水晶购买
								{
									int[] crystals=pi.player.removeCrystal(gcd.cost);
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买金币",crystals));
									brj.crystal =pi.player.getTotalCrystal();
								}
								else if(bj.costType ==2)//金币购买
								{
									pi.player.removeGold(gcd.cost);
									brj.crystal =pi.player.getGold();
								}
								brj.num =pi.player.getGold();
								brj.times =vData.gold-pi.player.getBuyGoldTimes();
								//记录每日活动进度
								ActivityController.updateTaskComplete(6, pi, 1);
								pi.player.addActive(6,1);
							}
						}
						else
						{
							brj.errorCode =errorcode1;
						}
						break;
					case 2://体力
						//校验阶段
						int errorcode2=0;
						if(pi.player.getBuyPowerTimes()>=vData.energy)//校验购买次数
						{
							errorcode2 =79;
						}
						if(errorcode2==0)
						{
							if(bj.jsonType==2)
							{
								
								//校验水晶或金币是否足够
								SpiritShopData ssd =SpiritShopData.getSpiritShopData(pi.player.getBuyPowerTimes()+1);
								if(ssd !=null)
								{
									if(bj.costType ==1)//花费水晶
									{
										if(pi.player.getTotalCrystal()<ssd.cost)
										{
											errorcode2 =19;
										}
									}
									else if(bj.costType ==2)//花费金币
									{
										if(pi.player.getGold()<ssd.cost)
										{
											errorcode2 =19;
										}
									}
									else
									{
										errorcode2 =80;
										errorlogger.info("花费类型错误");
									}
								}
								else
								{
									errorlogger.info("购买体力data为null");
								}
								if(errorcode2==0)
								{
									if(pi.player.getPower()>=Constant.MaxPower)
									{
										errorcode2 =51;
									}
								}
							}
						}
						
						//功能阶段
						if(errorcode2 ==0)
						{
							if(bj.jsonType==1)//购买请求
							{
								SpiritShopData ssd =SpiritShopData.getSpiritShopData(pi.player.getBuyPowerTimes()+1);
								brj.crystal =ssd.cost;
								brj.num =ssd.number;
								brj.times =vData.energy-pi.player.getBuyPowerTimes();
							}
							else if(bj.jsonType==2)//购买
							{
								SpiritShopData ssd =SpiritShopData.getSpiritShopData(pi.player.getBuyPowerTimes()+1);
								pi.player.addPower(ssd.number, false);
								pi.player.setBuyPowerTimes(pi.player.getBuyPowerTimes()+1);
								if(bj.costType==1)
								{
									int[] crystals=pi.player.removeCrystal(ssd.cost);
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买体力",crystals));
									brj.crystal =pi.player.getTotalCrystal();
								}
								else if(bj.costType==2)
								{
									pi.player.removeGold(ssd.cost);
									brj.crystal =pi.player.getGold();
								}
								brj.num =pi.player.getPower();
								brj.times =vData.energy-pi.player.getBuyPowerTimes();
								//记录每日活动进度
								ActivityController.updateTaskComplete(14, pi, 1);
								pi.player.addActive(14,1);
							}
						}
						else
						{
							brj.errorCode =errorcode2;
						}
						break;
					case 3://扫荡券
						int errorcode3=0;
						if(bj.jsonType==1)//购买请求
						{
							List<Item> items =pi.getTargetItems(80001);
							int needNum=0;//需购买的个数
							if(items!=null && items.size()>0)
							{
								needNum =bj.sweepTimes-pi.getTargetItemsNum(80001);
							}
							else
							{
								needNum =bj.sweepTimes;
							}
							HashMap<Integer, MissionCostData> mcMap =new HashMap<Integer, MissionCostData>();
							mcMap =MissionCostData.getMissionDatasByType(bj.type);
							MissionCostData mcData =mcMap.get(0);
							brj.crystal =mcData.cost*needNum;
							brj.num =needNum;
							pi.buyJson =brj;
						}
						if(bj.jsonType==2)
						{
							//校验阶段
							//校验水晶
							HashMap<Integer, MissionCostData> mcMap =new HashMap<Integer, MissionCostData>();
							mcMap =MissionCostData.getMissionDatasByType(bj.type);
							MissionCostData mcData =mcMap.get(0);
							if(bj.costType ==1)
							{
								if(pi.player.getTotalCrystal()<pi.buyJson.num*mcData.cost)
								{
									errorcode3 =19;
								}
							}
							else if(bj.costType==2)
							{
								if(pi.player.getGold()<pi.buyJson.num*mcData.cost)
								{
									errorcode3 =19;
								}
							}
							//功能阶段
							if(errorcode3 ==0)
							{
								pi.addItem(80001, pi.buyJson.num);
								if(bj.costType==1)
								{
									int[] crystals=pi.player.removeCrystal(pi.buyJson.num*mcData.cost);
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买扫荡券",crystals));
									brj.crystal =pi.player.getTotalCrystal();
								}
								if(bj.costType==2)
								{
									pi.player.removeGold(pi.buyJson.num*mcData.cost);
									brj.crystal =pi.player.getGold();
								}
								brj.num =pi.getTargetItemsNum(80001);
							}
							else
							{
								brj.errorCode =errorcode3;
							}
						}
						break;
					case 4://挑战次数
						int errorcode4=0;
						MissionData mData =MissionData.getMissionData(bj.md);
						if(mData !=null)
						{
							int sequence=mData.getSequence();
							//获取挑战次数
							int num=0;
							if(mData.missiontype ==1)
							{
								String times=pi.player.getBuyPveEntryTimes1();
								int beginIndex=(sequence-1)*2;
								int endIndex=beginIndex+2;
								String t=times.substring(beginIndex, endIndex);
								num=StringUtil.getInt(t);
							}
							else if(mData.missiontype ==2)
							{
								String times=pi.player.getBuyPveEntryTimes2();
								int beginIndex=(sequence-1)*2;
								int endIndex=beginIndex+2;
								String t=times.substring(beginIndex, endIndex);
								num=StringUtil.getInt(t);
							}
							if(bj.jsonType==1)
							{
								if(errorcode4 ==0)//校验vip等级
								{
									if(vData !=null)
									{
										if(mData.missiontype ==1)
										{
											if(vData.normal ==0)
											{
												errorcode4 =70;
											}
											else
											{
												if(num>=vData.normal)//校验购买次数
												{
													errorcode4 =79;
												}
											}
										}
										else if(mData.missiontype ==2)
										{
											if(vData.hero ==0)
											{
												errorcode4 =70;
											}
											else
											{
												if(num>=vData.hero)//校验购买次数
												{
													errorcode4 =79;
												}
											}
										}
									}
								}
								//功能阶段
								if(errorcode4==0)
								{
									MissionCostData mcData =MissionCostData.getMissionCostData(num+1);
									brj.crystal =mcData.cost;
									if(mData.missiontype ==1)
									{
										brj.times =vData.normal-num;
									}
									else if(mData.missiontype ==2)
									{
										brj.times =vData.hero-num;
									}
									
									brj.num =mData.times;
									brj.md =bj.md;
									pi.buyJson =brj;
								}
								else
								{
									brj.errorCode =errorcode4;
								}
							}
							if(bj.jsonType==2)
							{
								//校验阶段
								//校验购买次数
								if(mData.missiontype ==1)
								{
									if(num>=vData.normal)
									{
										errorcode4 =79;
									}
								}
								else if(mData.missiontype ==2)
								{
									if(num>=vData.hero)
									{
										errorcode4 =79;
									}
								}
								MissionCostData mcData =MissionCostData.getMissionCostData(num+1);
								//校验水晶
								if(errorcode4==0)
								{
									if(bj.costType==1)
									{
										if(pi.player.getTotalCrystal()<mcData.cost)
										{
											errorcode4 =19;
										}
									}
									else if(bj.costType==2)
									{
										if(pi.player.getGold()<mcData.cost)
										{
											errorcode4 =19;
										}
									}
								}
								//校验挑战次数
								int sequence1 =mData.getSequence();
								int num1 =0;
								if(mData.missiontype==1)
								{
									String times=pi.player.getTimes1();
									int beginIndex=(sequence1-1)*2;
									int endIndex=beginIndex+2;
									num1=StringUtil.getInt(times.substring(beginIndex, endIndex));
								}
								else
								{
									String times=pi.player.getTimes2();
									int beginIndex=(sequence1-1)*2;
									int endIndex=beginIndex+2;
									num1=StringUtil.getInt(times.substring(beginIndex, endIndex));
								}
								if(num1<mData.times)
								{
									errorcode4=84;
								}
								
								
								//功能阶段
								if(errorcode4==0)
								{
									//改变挑战次数
									if(mData !=null)
									{
										if(mData.missiontype==1)
										{
											String times=pi.player.getTimes1();
											int beginIndex=(sequence-1)*2;
											int endIndex=beginIndex+2;
											String s=0+"";
											if(s.length()==1)
											{
												s="0"+s;
											}
											times=times.substring(0, beginIndex)+s+times.substring(endIndex, times.length());
											pi.player.setTimes1(times);
										}
										else
										{
											String times=pi.player.getTimes2();
											int beginIndex=(sequence-1)*2;
											int endIndex=beginIndex+2;
											String s=0+"";
											if(s.length()==1)
											{
												s="0"+s;
											}
											times=times.substring(0, beginIndex)+s+times.substring(endIndex, times.length());
											pi.player.setTimes2(times);
										}
										brj.num =mData.times;
										if(mcData !=null)
										{
											//扣除水晶
											 if(bj.costType==1)
											{
												int[] crystals=pi.player.removeCrystal(mcData.cost);
												MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买副本挑战次数|"+(mData.missiontype==1?"普通":"精英"),crystals));
												brj.crystal =pi.player.getTotalCrystal();
											}
											else if(bj.costType ==2)
											{
												pi.player.removeGold(mcData.cost);
												brj.crystal =pi.player.getGold();
											}
										}
										if(mData.missiontype ==1)
										{
											String times=pi.player.getBuyPveEntryTimes1();//修改数据库
											int beginIndex=(sequence-1)*2;
											int endIndex=beginIndex+2;
											String t=times.substring(beginIndex, endIndex);
											int num2=StringUtil.getInt(t);
											String s=(num2+1)+"";
											if(s.length()==1)
											{
												s="0"+s;
											}
											times=times.substring(0, beginIndex)+s+times.substring(endIndex, times.length());
											pi.player.setBuyPveEntryTimes1(times);
											brj.times =vData.normal-num;
										}
										else if(mData.missiontype ==2)
										{
											String times=pi.player.getBuyPveEntryTimes2();//修改数据库
											int beginIndex=(sequence-1)*2;
											int endIndex=beginIndex+2;
											String t=times.substring(beginIndex, endIndex);
											int num2=StringUtil.getInt(t);
											String s=(num2+1)+"";
											if(s.length()==1)
											{
												s="0"+s;
											}
											times=times.substring(0, beginIndex)+s+times.substring(endIndex, times.length());
											pi.player.setBuyPveEntryTimes2(times);
											brj.times =vData.hero-num;
										}
										
									}
								}
								else
								{
									brj.errorCode =errorcode4;
								}
							}
						}
						else
						{
							errorlogger.info("mdata is null");
						}
						break;
					case 5://立即挑战(购买冷却时间)
						//校验阶段
						int errorcode5 =0;
						long curTime =System.currentTimeMillis();
						if(bj.jsonType==1)
						{
							//校验冷却时间
							int cdtime=0;
							long time=0;
							int constantTime =10*60;
							if(bj.cdType==1)//pk
							{
								time =pi.pkcdtime;
								constantTime =5*60;
							}
							else if(bj.cdType==2)//maze
							{
								time =pi.mazecdtime;
							}
							else if(bj.cdType==3)//event
							{
								EventData eData =EventData.getEventData(bj.eventId);
								if(eData !=null)
								{
									if(eData.positionid==1)
									{
										if(pi.eventcdtime1==0)
										{
											errorcode5 =81;
										}
										else
										{
											time =pi.eventcdtime1;
										}
									}
									else if(eData.positionid ==2)
									{
										if(pi.eventcdtime2==0)
										{
											errorcode5 =81;
										}
										else
										{
											time =pi.eventcdtime2;
										}
									}
									else if(eData.positionid==3)
									{
										if(pi.eventcdtime3==0)
										{
											errorcode5 =81;
										}
										else
										{
											time =pi.eventcdtime3;
										}
										
									}
									else
									{
										errorlogger.info("购买冷却时间时，eventData 中的positionId is error");
									}
								}
								else
								{
									errorlogger.info("购买冷却时间时，eventData is null");
								}
							}
							if(time==0)
							{
								errorcode5 =81;
							}
							else
							{
								cdtime =(int)(curTime-time)/1000;
								if(cdtime>=constantTime)
								{
									errorcode5 =81;
								}
							}
							//功能阶段
							if(errorcode5==0)
							{
								cdtime =(int)(curTime-time)/1000;
								if((constantTime-cdtime)%60==0)
								{
									brj.crystal =(constantTime-cdtime)/60;
								}
								else
								{
									brj.crystal =(constantTime-cdtime)/60+1;
								}
								pi.bJson =bj;
								pi.buyJson =brj;
							}
							else
							{
								brj.errorCode =errorcode5;
							}
						}
						if(bj.jsonType==2)
						{
							//校验阶段
							//校验水晶
							long time =0;
							int cdtime=0;
							int constantTime=10*60;
							if(errorcode5==0)
							{
								if(pi.player.getTotalCrystal()<pi.buyJson.crystal)
								{
									errorcode5 =19;
								}
							}
							//功能阶段
							if(errorcode5==0)
							{
								if(pi.bJson.cdType==1)//pk
								{
									time =pi.pkcdtime;
									pi.pkcdtime=0;
									constantTime =5*60;
								}
								else if(pi.bJson.cdType==2)
								{
									time =pi.mazecdtime;
									pi.mazecdtime =0;
								}
								else if(pi.bJson.cdType==3)
								{
									EventData eData =EventData.getEventData(pi.bJson.eventId);
									if(eData.positionid==1)
									{
										time =pi.eventcdtime1;
										pi.eventcdtime1=0;
									}
									else if(eData.positionid==2)
									{
										time =pi.eventcdtime2;
										pi.eventcdtime2=0;
									}
									else if(eData.positionid ==3)
									{
										time =pi.eventcdtime3;
										pi.eventcdtime3=0;
									}
								}
								cdtime=(int)(curTime-time)/1000;
								if(cdtime<constantTime)
								{
									String name="未知";
									if(pi.bJson.cdType==1)
									{
										name="PVP";
									}
									else if(pi.bJson.cdType==2)
									{
										name="迷宫";
									}
									else if(pi.bJson.cdType==3)
									{
										name="异世界";
									}
									
									if((constantTime-cdtime)%60==0)//扣除水晶
									{
										int[] crystals =null;
										if(pi.bJson.cdType==1)//pvp扣除水晶减半
										{
											crystals=pi.player.removeCrystal(((constantTime-cdtime)/60)/2);
										}
										else
										{
											crystals=pi.player.removeCrystal((constantTime-cdtime)/60);
										}
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买冷却时间|"+name,crystals));
									}
									else
									{
										int[] crystals =null;
										if(pi.bJson.cdType==1)//pvp扣除水晶减半
										{
											crystals=pi.player.removeCrystal(((constantTime-cdtime)/60+1)/2);
										}
										else
										{
											crystals =pi.player.removeCrystal((constantTime-cdtime)/60+1);
										}
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买冷却时间|"+name,crystals));
									}
								}
								brj.crystal =pi.player.getTotalCrystal();
							}	
							else
							{
								brj.errorCode =errorcode5;
							}
						}
						break;
					case 6://pvp进入次数
						int errorcode6=0;
						if(bj.jsonType ==1)//购买请求
						{
							//校验阶段
							if(errorcode6 ==0)
							{
								PkRank pr =Cache.getInstance().getRankById(pi.player.getId());
								if(pr==null || pr.getPkNum()<10+pi.player.getBuyPkNumTimes())
								{
									errorcode6 =84;
								}
							}
							if(errorcode6 ==0)//校验vip等级
							{
								if(vData !=null)
								{
									if(vData.pvptime==0)
									{
										errorcode6 =70;
									}
								}
							}
							//功能阶段
							if(errorcode6 ==0)
							{
									brj.crystal =2*(pi.player.getBuyPkNumTimes()+1);
									brj.num =1;
							}
							else
							{
								brj.errorCode =errorcode6;
							}
						}
						else if(bj.jsonType ==2)//购买
						{
							//校验阶段
							if(errorcode6 ==0)
							{
								PkRank pr =Cache.getInstance().getRankById(pi.player.getId());
								if(pr==null || pr.getPkNum()<10+pi.player.getBuyPkNumTimes())
								{
									errorcode6 =84;
								}
							}
							if(errorcode6 ==0)//校验vip等级
							{
								if(vData !=null)
								{
									if(vData.pvptime==0)
									{
										errorcode6 =70;
									}
								}
							}
							if(errorcode6 ==0)
							{
								//校验金币或者水晶
								if(pi.player.getTotalCrystal()<2*(pi.player.getBuyPkNumTimes()+1))
								{
									errorcode6 =19;
								}
							}
							//功能阶段
							if(errorcode6 ==0)
							{
									int[] crystals=pi.player.removeCrystal(2*(pi.player.getBuyPkNumTimes()+1));
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买PVP次数",crystals));
									pi.player.setBuyPkNumTimes(pi.player.getBuyPkNumTimes()+1);
									brj.crystal =pi.player.getTotalCrystal();
							}
							else
							{
								brj.errorCode =errorcode6;
							}
						}
						break;
					case 7://购买vip礼包
						int errorcode7=0;
						if(bj.jsonType==2)
						{
							String [] temp =null;
							if(pi.player.getBuyVipGift() !=null && pi.player.getBuyVipGift().length()>0)
							{
								temp =pi.player.getBuyVipGift().split("-");
							}
							HashMap<Integer, String> vipGiftMap =new HashMap<Integer, String>();
							if(temp !=null)
							{
								for(int j=0;j<temp.length;j++)
								{
									if(temp[j] !=null && temp[j].length()>0)
									{
										if(!vipGiftMap.containsKey(StringUtil.getInt(temp[j])))
										{
											vipGiftMap.put(StringUtil.getInt(temp[j]), temp[j]);
										}
									}
								}
							}
							
							//校验
							int giftId=bj.giftId;
							if(errorcode7 ==0)
							{
								if(giftId>vData.giftid)//校验vip等级
								{
									errorcode7 =70;
								}
							}
							
							if(errorcode7 ==0)
							{
								if(vipGiftMap.containsKey(giftId))//校验是否购买过
								{
									errorcode7 =104;
								}
							}
							//校验水晶
							VipData vData1 =VipData.getVipData(giftId);
							if(errorcode7 ==0)
							{
								if(bj.costType ==1)
								{
									if(vData1.realprice>pi.player.getTotalCrystal())
									{
										errorcode7 =19;
									}
								}
								else if(bj.costType ==2)
								{
									if(vData1.realprice >pi.player.getGold())
									{
										errorcode7 =19;
									}
								}
							}
							//功能阶段
							if(errorcode7 ==0)
							{
								VipGiftData vgData =VipGiftData.getVipGiftData(giftId);
								List<String> list =vgData.goods;
								//扣除水晶或者金币
								if(bj.costType ==1)
								{
									int[] crystals=pi.player.removeCrystal(vData1.realprice);
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买VIP礼包|"+giftId,crystals));
									brj.crystal =pi.player.getTotalCrystal();
								}
								else if(bj.costType ==2)
								{
									pi.player.removeGold(vData1.realprice);
									pi.player.getGold();
								}
								List<Integer> cardIds =new ArrayList<Integer>();
								//发放物品
								for(int j=0;j<list.size();j++)
								{
									String str =list.get(j);
									
									if(str !=null && str.length()>0)
									{
										String [] ss =str.split("-");
										if(ss[0] !=null && ss[0].length()>0)
										{
											if(StringUtil.getInt(ss[0])==9)
											{
												Statics.getReward(11, ss[1], pi);
											}
											else
											{
												if(StringUtil.getInt(ss[0])==3)
												{
													String [] stri =ss[1].split(",");
													cardIds.add(StringUtil.getInt(stri[0]));
												}
												Statics.getReward(StringUtil.getInt(ss[0]), ss[1], pi);
											}
										}
									}
								}
								if(cardIds.size()>0)
								{
									pi.getNewUnitSkill(cardIds);
								}
								//修改数据库
								if(pi.player.getBuyVipGift()==null || pi.player.getBuyVipGift().length()==0)
								{
									pi.player.setBuyVipGift(giftId+"");
								}
								else
								{
									pi.player.setBuyVipGift(pi.player.getBuyVipGift()+"-"+giftId);
								}
								String vipGift =pi.player.getBuyVipGift();
								List<String> giftIds =new ArrayList<String>();
								if(vipGift !=null && vipGift.length()>0)
								{
									String [] ss =vipGift.split("-");
									HashMap<Integer, String> map =new HashMap<Integer, String>();
									for(int i=0;i<ss.length;i++)
									{
										if(ss[i] !=null && ss[i].length()>0)
										{
											if(!map.containsKey(StringUtil.getInt(ss[i])))
											{
												map.put(StringUtil.getInt(ss[i]), ss[i]);
											}
										}
									}
									for(int i=1;i<=pi.player.getVipLevel();i++)
									{
										if(!map.containsKey(i))
										{
											giftIds.add(i+"");
										}
									}
								}
								else
								{
									for(int i=1;i<=pi.player.getVipLevel();i++)
									{
										giftIds.add(i+"");
									}
								}
								brj.ids =giftIds;
								
							}
							else
							{
								brj.errorCode =errorcode7;
							}
						}
						break;
					case 8://商城或者黑市的购买
						if(bj.jsonType==2)
						{
							int errorcode8 =0;
							int goosId =bj.goodsId;
							if(bj.shopType==1)//商城
							{
								//校验
								ShopData sData =ShopData.getShopData(goosId);
								//校验vip
								if(errorcode8 ==0)
								{
									if(pi.player.getVipLevel()<sData.viplevel)
									{
										errorcode8 =70;
									}
								}
								//校验cost
								if(errorcode8 ==0)
								{
									if(sData.costtype1 ==1)
									{
										if(pi.player.getTotalCrystal()<sData.cost*bj.number)
										{
											errorcode8 =19;
										}
									}
									else if(sData.costtype1==2)
									{
										if(pi.player.getGold()<sData.cost*bj.number)
										{
											errorcode8 =89;
										}
									}
								}
								//校验购买次数
								if(errorcode8 ==0)
								{
									if(sData.dailynumber>0)
									{
										HashMap<Integer, Integer> shopMap =pi.getBuyShopNumMap();
										if(shopMap.containsKey(goosId))
										{
											if(sData.dailynumber-shopMap.get(goosId)<bj.number)
											{
												errorcode8=79;
											}
										}
									}
									if(sData.totalnumber>0)
									{
										HashMap<Integer, Integer> shopMap =pi.getBuyShopNumsMap();
										if(shopMap.containsKey(goosId))
										{
											if(sData.totalnumber-shopMap.get(goosId)<bj.number)
											{
												errorcode8 =79;
											}
										}
									}
								}
								//功能阶段
								if(errorcode8 ==0)
								{
									List<Integer> cardIds =new ArrayList<Integer>();
									//发放物品
									switch (sData.goodstype)
									{
									case 1://item
										pi.addItem(sData.itemId, bj.number);
										break;
									case 2://equip
										for(int i=0;i<bj.number;i++)
										{
											pi.addEquip(sData.itemId);
										}
										break;
									case 3://card
										cardIds.add(sData.itemId);
										for(int i=0;i<bj.number;i++)
										{
											pi.addCard(sData.itemId, 1);
										}
										break;
									case 4://skill
										for(int i=0;i<bj.number;i++)
										{
											pi.addSkill(sData.itemId, 1);
										}
										break;
									case 5://passiveskill
										for(int i=0;i<bj.number;i++)
										{
											pi.addPassiveSkill(sData.itemId);
										}
										break;
									}
									if(cardIds.size()>0)
									{
										pi.getNewUnitSkill(cardIds);
									}
									//扣钱
									if(sData.costtype1==1)
									{
										int[] crystals=pi.player.removeCrystal(sData.cost*bj.number);
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买商城物品|"+sData.name,crystals));
									}
									else if(sData.costtype1==2)
									{
										pi.player.removeGold(sData.cost*bj.number);
									}
									//修改购买次数
									if(sData.dailynumber>0)//每日购买次数
									{
										HashMap<Integer, Integer> shopMap =pi.getBuyShopNumMap();
										if(shopMap.containsKey(sData.id))
										{
											String buyShopNum =pi.player.getBuyShopNum();
											String [] str =buyShopNum.split("-");
											String newStr ="";
											boolean a=false;
											for(int i=0;i<str.length;i++)
											{
												String [] temp =str[i].split("&");
												if(!a)
												{
													if(StringUtil.getInt(temp[0])==sData.id)
													{
														str[i] =temp[0]+"&"+(StringUtil.getInt(temp[1])+bj.number);
														a =true;
													}
												}
												
												if(newStr ==null || newStr.length()==0)
												{
													newStr =str[i];
												}
												else
												{
													newStr =newStr +"-"+str[i];
												}
											}
											pi.player.setBuyShopNum(newStr);
										}
										else
										{
											if(pi.player.getBuyShopNum() ==null || pi.player.getBuyShopNum().length()==0)
											{
												pi.player.setBuyShopNum(sData.id+"&"+bj.number);
											}
											else
											{
												pi.player.setBuyShopNum(pi.player.getBuyShopNum()+"-"+sData.id+"&"+bj.number);
											}
										}
									}
									if(sData.totalnumber>0)//总的购买次数
									{
										HashMap<Integer, Integer> shopMap =pi.getBuyShopNumsMap();
										if(shopMap.containsKey(sData.id))
										{
											String buyShopNums =pi.player.getBuyShopNums();
											String [] str =buyShopNums.split("-");
											String newStr ="";
											boolean a=false;;
											for(int i=0;i<str.length;i++)
											{
												String [] temp =str[i].split("&");
												if(a)
												{
													if(StringUtil.getInt(temp[0])==sData.id)
													{
														str[i] =temp[0]+"&"+(StringUtil.getInt(temp[1])+bj.number);
														a =false;
													}
												}
												
												if(newStr ==null || newStr.length()==0)
												{
													newStr =str[i];
												}
												else
												{
													newStr =newStr +"-"+str[i];
												}
											}
											pi.player.setBuyShopNums(newStr);
										}
										else
										{
											if(pi.player.getBuyShopNums() ==null || pi.player.getBuyShopNums().length()==0)
											{
												pi.player.setBuyShopNums(sData.id+"&"+bj.number);
											}
											else
											{
												pi.player.setBuyShopNums(pi.player.getBuyShopNums()+"-"+sData.id+"&"+bj.number);
											}
										}
									}
									brj.crystal =pi.player.getTotalCrystal();
									brj.goodsInfo =sData.goodstype+"-"+sData.itemId;
								}
								else
								{
									brj.errorCode =errorcode8;
								}
							}
							else if(bj.shopType ==2)//黑市
							{
								BlackMarketData bmData =BlackMarketData.getBlackMarketData(goosId);
								//校验
								//校验花费
								if(errorcode8 ==0)
								{
									if(bmData.costtype==1)
									{
										if(pi.player.getTotalCrystal()<bmData.cost)
										{
											errorcode8 =19;
										}
									}
									else if(bmData.costtype==2)
									{
										if(pi.player.getGold()<bmData.cost)
										{
											errorcode8 =89;
										}
									}
								}
								//校验是否存在此物品或者物品的购买次数
								String [] shopList =null;
								shopList =pi.player.getBlackMarkets().split("&");
								boolean inlist =false;
								if(errorcode8 ==0)
								{
									for(int i=0;i<shopList.length;i++)
									{
										String info =shopList[i];
										String [] temp =info.split("-");
										if(StringUtil.getInt(temp[0])==goosId)
										{
											inlist =true;
											if(StringUtil.getInt(temp[1])>=1)
											{
												errorcode8 =79;
											}
										}
									}
									if(!inlist)
									{
										errorcode8 =21;
									}
								}
								//功能阶段
								if(errorcode8 ==0)
								{
									List<Integer> cardIds =new ArrayList<Integer>();
									//发放物品
									switch (bmData.goodstype)
									{
									case 1://item
										pi.addItem(bmData.itemId, bmData.number);
										break;
									case 2://equip
										for(int i=0;i<bmData.number;i++)
										{
											pi.addEquip(bmData.itemId);
										}
										break;
									case 3://card
										cardIds.add(bmData.itemId);
										for(int i=0;i<bmData.number;i++)
										{
											pi.addCard(bmData.itemId, 1);
										}
										break;
									case 4://skill
										for(int i=0;i<bmData.number;i++)
										{
											pi.addSkill(bmData.itemId, 1);
										}
										break;
									case 5://pskill
										for(int i=0;i<bmData.number;i++)
										{
											pi.addPassiveSkill(bmData.itemId);
										}
										break;
									case 6://gold
										pi.player.addGold(bmData.number);
										break;
									case 7://personexp
										pi.player.addExp(bmData.number);
										break;
									case 8://cratal
										pi.player.addCrystal(bmData.number);
										break;
									}
									if(cardIds.size()>0)
									{
										pi.getNewUnitSkill(cardIds);
									}
									//扣钱
									if(bmData.costtype ==1)
									{
										int[] crystals=pi.player.removeCrystal(bmData.cost);
										MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买黑市物品|"+bmData.name,crystals));
										brj.crystal =pi.player.getTotalCrystal();
									}
									else if(bmData.costtype==2)
									{
										pi.player.removeGold(bmData.cost);
										brj.crystal =pi.player.getGold();
										
									}
									//修改购买次数
									String s="";
									for(int i=0;i<shopList.length;i++)
									{
										String str =shopList[i];
										String [] temp =str.split("-");
										if(StringUtil.getInt(temp[0])==goosId)
										{
											shopList[i] =temp[0]+"-"+(StringUtil.getInt(temp[1])+1);
										}
										if(s ==null || s.length()==0)
										{
											s =shopList[i];
										}
										else
										{
											s =s+"&"+shopList[i];
										}
									}
									pi.player.setBlackMarkets(s);
									brj.goodsInfo =bmData.goodstype+"-"+bmData.itemId;
								}
								else
								{
									brj.errorCode =errorcode8;
								}
							}else if(bj.shopType == 3){
								// 竞技场商城
								ShoppvpData sd = ShoppvpData
										.getShoppvpData(goosId);
								// 校验
								// 校验花费
								if (errorcode8 == 0) {
									if (sd.costtype == 1) {
										if (pi.player.getTotalCrystal() < sd.cost) {
											errorcode8 = 19;
										}
									} else if (sd.costtype == 2) {
										if (pi.player.getGold() < sd.cost) {
											errorcode8 = 89;
										}
									} else if (sd.costtype == 3) {
										if (pi.player.getPvpHonor() < sd.cost) {
											errorcode8 = 125;
										}
									}
								}
								// 校验是否存在此物品或者物品的购买次数
								String[] shopList = null;
								shopList = pi.player.getPvpShop().split(",");
								boolean inlist = false;
								if (errorcode8 == 0) {
									for (int i = 0; i < shopList.length; i++) {
										String info = shopList[i];
										String[] temp = info.split("-");
										if (StringUtil.getInt(temp[0]) == goosId) {
											inlist = true;
											if (StringUtil.getInt(temp[1]) >= 1) {
												errorcode8 = 79;
											}
										}
									}
									if (!inlist) {
										errorcode8 = 21;
									}
								}
								// 功能阶段{
								if (errorcode8 == 0) {
									List<Integer> cardIds = new ArrayList<Integer>();
									// 发放物品
									switch (sd.goodstype) {
									case 1:// item
										pi.addItem(sd.itemId, sd.number);
										break;
									case 2:// equip
										for (int i = 0; i < sd.number; i++) {
											pi.addEquip(sd.itemId);
										}
										break;
									case 3:// card
										cardIds.add(sd.itemId);
										for (int i = 0; i < sd.number; i++) {
											pi.addCard(sd.itemId, 1);
										}
										break;
									case 4:// skill
										for (int i = 0; i < sd.number; i++) {
											pi.addSkill(sd.itemId, 1);
										}
										break;
									case 5:// pskill
										for (int i = 0; i < sd.number; i++) {
											pi.addPassiveSkill(sd.itemId);
										}
										break;
									case 6:// gold
										pi.player.addGold(sd.number);
										break;
									case 7:// personexp
										pi.player.addExp(sd.number);
										break;
									case 8:// cratal
										pi.player.addCrystal(sd.number);
										break;
									case 9:// diamond
										pi.player.addDiamond(sd.number);
										break;
									}
									if (cardIds.size() > 0) {
										pi.getNewUnitSkill(cardIds);
									}
									// 扣钱
									if (sd.costtype == 1) {
										int[] crystals = pi.player
												.removeCrystal(sd.cost);
										MailThread.getInstance().addLogbuy(
												LogBuy.createLogBuy(pi.player
														.getId(), "购买竞技场商城物品|"
														+ sd.name, crystals));
										brj.crystal = pi.player
												.getTotalCrystal();
									} else if (sd.costtype == 2) {
										pi.player.removeGold(sd.cost);
										brj.crystal = pi.player.getGold();

									} else if (sd.costtype == 3) {
										pi.player.addPvpHonor(-sd.cost);
										brj.crystal = pi.player.getPvpHonor();

									}
									// 修改购买次数
									String s = "";
									for (int i = 0; i < shopList.length; i++) {
										String str = shopList[i];
										String[] temp = str.split("-");
										if (StringUtil.getInt(temp[0]) == goosId) {
											shopList[i] = temp[0]+ "-"+ (StringUtil.getInt(temp[1]) + 1);
										}
										if (s == null || s.length() == 0) {
											s = shopList[i];
										} else {
											s = s + "," + shopList[i];
										}
									}
									pi.player.setPvpShop(s);
									brj.goodsInfo = sd.goodstype + "-"+ sd.itemId;
								} else {
									brj.errorCode = errorcode8;
								}
							}
						}
						break;
					case 9://购买黑市位置格子
						int errorcode9=0;
						
						if(bj.jsonType==2)
						{
							//校验购买次数
							if(pi.player.getBuyShopBoxNum()>=BlackShopBoxData.getDataSize())
							{
								errorcode9=79;
							}
							//校验花费
							BlackShopBoxData bsData1 =BlackShopBoxData.getBlackShopBoxData(pi.player.getBuyShopBoxNum()+1+BlackShopBoxData.getShopBoxNum());
							if(errorcode9==0)
							{
								
								if(bsData1.costtype==1)
								{
									if(pi.player.getTotalCrystal()<bsData1.cost)
									{
										errorcode9=19;
									}
								}
								else if(bsData1.costtype==2)
								{
									if(pi.player.getGold()<bsData1.cost)
									{
										errorcode9=89;
									}
								}
							}
							//校验vip
							if(errorcode9 ==0)
							{
								if(pi.player.getVipLevel()<bsData1.viplevel)
								{
									errorcode9 =70;
								}
							}
							//功能阶段
							if(errorcode9==0)
							{
								//扣钱
								BlackShopBoxData bsData =BlackShopBoxData.getBlackShopBoxData(pi.player.getBuyShopBoxNum()+1+BlackShopBoxData.getShopBoxNum());
								if(bsData.costtype==1)
								{
									int[] crystals=pi.player.removeCrystal(bsData.cost);
									MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买黑市格子",crystals));
								}
								else if(bsData.costtype==2)
								{
									pi.player.removeGold(bsData.cost);
								}
								//修改次数
								pi.player.setBuyShopBoxNum(pi.player.getBuyShopBoxNum()+1);
								//为新位置随机出物品
								int randNum =Random.getNumber(0, Constant.TotalDropPro);
								int st =0;
								int end =0;
								boolean a =true;
								List<BlackMarketData> list =BlackMarketData.getDataList();
								String [] list2=null;
								if(pi.player.getBlackMarkets() !=null && pi.player.getBlackMarkets().length()>0)
								{
									list2 =pi.player.getBlackMarkets().split("&");
								}
								HashMap<Integer, Integer> blackMap =new HashMap<Integer, Integer>();
								if(list2 !=null && list2.length>0)
								{
									for(int i=0;i<list2.length;i++)
									{
										String [] temp =list2[i].split("-");
										if(!blackMap.containsKey(StringUtil.getInt(temp[0])))
										{
											blackMap.put(StringUtil.getInt(temp[0]), StringUtil.getInt(temp[0]));
										}
										else
										{
											errorlogger.info("blacklist is error");
										}
									}
								}
								int j=0;
								for(int k=0;k<1+j;k++)
								{
									for(BlackMarketData bmData :list)
									{
										if(pi.player.getLevel()>=bmData.level)
										{
											end =bmData.probability1+st;
											if(randNum>=st && randNum<=end)
											{
												if(blackMap.containsKey(bmData.id))
												{
													j++;
												}
												else
												{
													brj.md =bmData.id;
												}
												a =false;
												break;
											}
											else
											{
												st =end;
											}
										}
									}
									if(a)
									{
										if(blackMap.containsKey(list.get(0).id))
										{
											j++;
										}
										else
										{
											brj.md =list.get(0).id;
										}
									}
								}
								brj.crystal =pi.player.getTotalCrystal();
								if(brj.md!=0)
								{
									if(pi.player.getBlackMarkets() !=null && pi.player.getBlackMarkets().length()>0)
									{
										pi.player.setBlackMarkets(pi.player.getBlackMarkets()+"&"+brj.md+"-"+0);
									}
									else
									{
										pi.player.setBlackMarkets(brj.md+"-"+0);
									}
								}
							}
							else
							{
								brj.errorCode =errorcode9;
							}
						}
						break;
					case 10://购买血瓶
						int errorcode10=0;
						//校验阶段
						if(bj.jsonType==1)
						{
							if(pi.getTargetItemsNum(130001)>0)
							{
								errorcode10 =123;
							}
							//功能阶段
							if(errorcode10==0)
							{
								int cost=0;
								if(pi.player.getBloodBuffNum()>=10)
								{
									cost =100;
								}
								else
								{
									BloodCostData bcd =BloodCostData.getBloodCostData(pi.player.getBloodBuffNum()+1);
									cost =bcd.cost;
								}
								brj.crystal =cost;
							}
							else
							{
								brj.errorCode =errorcode10;
							}
						}
						else if(bj.jsonType==2)
						{
							//校验阶段
							int cost=0;
							if(errorcode10 ==0)
							{
								if(pi.player.getBloodBuffNum()>=10)
								{
									cost =100;
								}
								else
								{
									BloodCostData bcd =BloodCostData.getBloodCostData(pi.player.getBloodBuffNum()+1);
									cost =bcd.cost;
								}
								if(errorcode10 ==0)
								{
									if(pi.getTargetItemsNum(130001)>0)
									{
										errorcode10 =123;
									}
								}
								if(errorcode10 ==0)
								{
									if(pi.player.getTotalCrystal()<cost)
									{
										errorcode10=19;
									}
								}
							}
							//功能阶段
							if(errorcode10==0)
							{
								pi.player.setBloodBuffNum(pi.player.getBloodBuffNum()+1);
								pi.player.removeCrystal(cost);
								pi.addItem(130001, 1);
								brj.crystal =pi.player.getTotalCrystal();
								brj.num =pi.getTargetItemsNum(130001);
							}
							else
							{
								brj.errorCode =errorcode10;
							}
						}
						break;
						
					default:
						brj.errorCode =80;
						errorlogger.info("购买类型错误");
						break;
					}
				}
				else
				{
					brj.errorCode =-3;
				}
			}
			else
			{
				brj.errorCode =-1;
			}
			Cache.recordRequestNum(brj);
			String msg=JSON.toJSONString(brj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
	}
}
