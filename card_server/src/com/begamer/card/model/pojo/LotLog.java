package com.begamer.card.model.pojo;

public class LotLog {

	public int id;
	public int playerId;
	public int chouCardNum;
	public String logDate;
	
	public static LotLog createChouCardLog(int playerId,int chouCardNum,String logDate)
	{
		LotLog chouCardLog = new LotLog();
		chouCardLog.setPlayerId(playerId);
		chouCardLog.setChouCardNum(chouCardNum);
		chouCardLog.setLogDate(logDate);
		return chouCardLog;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public int getChouCardNum() {
		return chouCardNum;
	}

	public void setChouCardNum(int chouCardNum) {
		this.chouCardNum = chouCardNum;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}
	
	
}
