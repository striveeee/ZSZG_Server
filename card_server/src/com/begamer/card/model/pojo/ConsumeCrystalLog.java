package com.begamer.card.model.pojo;

public class ConsumeCrystalLog {

	public int id;
	public int playerId;
	public int crystalLog;
	public String logDate;
	
	public static ConsumeCrystalLog createConsumeCrystalLog(int playerId,int crystalLog,String logDate)
	{
		ConsumeCrystalLog consumeCrystalLog = new ConsumeCrystalLog();
		consumeCrystalLog.setPlayerId(playerId);
		consumeCrystalLog.setCrystalLog(crystalLog);
		consumeCrystalLog.setLogDate(logDate);
		return consumeCrystalLog;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	
	public int getCrystalLog()
	{
		return crystalLog;
	}
	public void setCrystalLog(int crystalLog)
	{
		this.crystalLog = crystalLog;
	}
	public String getLogDate()
	{
		return logDate;
	}
	public void setLogDate(String logDate)
	{
		this.logDate = logDate;
	}
	
	
}
