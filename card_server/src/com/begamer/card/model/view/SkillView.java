package com.begamer.card.model.view;

public class SkillView {

	private int id;
	private String name;
	private int level;
	private String description;

	public static SkillView createSkillView(int id, String name, int level,
			String description)
	{
		SkillView skillView = new SkillView();
		skillView.setId(id);
		skillView.setName(name);
		skillView.setLevel(level);
		skillView.setDescription(description);
		return skillView;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

}
