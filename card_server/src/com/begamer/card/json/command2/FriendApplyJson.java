package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendApplyJson extends BasicJson
{
	/**索引或playerId**/
	public int i;
	//==0列表申请,1搜索申请,2支援玩家申请==//
	public int s;

	public int getI()
	{
		return i;
	}

	public void setI(int i)
	{
		this.i = i;
	}

	public int getS()
	{
		return s;
	}

	public void setS(int s)
	{
		this.s = s;
	}

	
}
