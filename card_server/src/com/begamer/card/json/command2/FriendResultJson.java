package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.FriendElement;

public class FriendResultJson extends ErrorJson
{
	public List<FriendElement> list;
	public int power;//当前体力
	public int times;//==今日已经使用的邀请好友次数==//
	public int cost;//==下一次邀请需要消耗的金币==//
	public int buyNum;//==购买次数==//
	
	public List<FriendElement> getList()
	{
		return list;
	}

	public void setList(List<FriendElement> list)
	{
		this.list = list;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public int getTimes()
	{
		return times;
	}

	public void setTimes(int times)
	{
		this.times = times;
	}

	public int getCost()
	{
		return cost;
	}

	public void setCost(int cost)
	{
		this.cost = cost;
	}

	public int getBuyNum()
	{
		return buyNum;
	}

	public void setBuyNum(int buyNum)
	{
		this.buyNum = buyNum;
	}
	
}
