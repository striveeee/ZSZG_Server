package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class GiftCodeResultJson extends ErrorJson
{
	public int gold;//金币
	public int crystal;//水晶
	public int runeNum;//符文值
	public int power;//体力
	public String card;//格式:cardId,cardId
	public String skill;//主动技能:skillId,skillId
	public String pSkill;//被动技能:pSkillId,pSkillId
	public String equip;//装备:equipId,equipId
	public String item;//材料:itemId,itemId
	
	public void setData(GiftJson gj)
	{
		gold=gj.gold;
		crystal=gj.crystal;
		runeNum=gj.runeNum;
		power=gj.power;
		card=gj.card;
		skill=gj.skill;
		pSkill=gj.pSkill;
		equip=gj.equip;
		item=gj.item;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

	public int getRuneNum()
	{
		return runeNum;
	}

	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public String getCard()
	{
		return card;
	}

	public void setCard(String card)
	{
		this.card = card;
	}

	public String getSkill()
	{
		return skill;
	}

	public void setSkill(String skill)
	{
		this.skill = skill;
	}

	public String getPSkill()
	{
		return pSkill;
	}

	public void setPSkill(String pSkill)
	{
		this.pSkill = pSkill;
	}

	public String getEquip()
	{
		return equip;
	}

	public void setEquip(String equip)
	{
		this.equip = equip;
	}

	public String getItem()
	{
		return item;
	}

	public void setItem(String item)
	{
		this.item = item;
	}
}
