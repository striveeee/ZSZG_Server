package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class SignResultJson extends ErrorJson
{
	//==本月签到次数==//
	public int times;
	//==今日签到:1已签到,0未签到==//
	public int mark;
	//==本月天数==//
	public int days;
	
	public int getTimes()
	{
		return times;
	}

	public void setTimes(int times)
	{
		this.times = times;
	}

	public int getMark()
	{
		return mark;
	}

	public void setMark(int mark)
	{
		this.mark = mark;
	}

	public int getDays()
	{
		return days;
	}

	public void setDays(int days)
	{
		this.days = days;
	}
	
}
