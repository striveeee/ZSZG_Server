package com.begamer.card.json.command2;

import java.util.List;
import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.TaskElement;

public class ActivityRewardResultJson extends ErrorJson
{
	public List<TaskElement> tes;//任务列表
	public int active;//活跃度
	public String activeState;//活跃度礼包领取状态
	public List<TaskElement> getTes() {
		return tes;
	}
	public void setTes(List<TaskElement> tes) {
		this.tes = tes;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public String getActiveState() {
		return activeState;
	}
	public void setActiveState(String activeState) {
		this.activeState = activeState;
	}
}
